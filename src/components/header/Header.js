import React from 'react';
import './Header.css';

/**
 * Functional component to display a header
 * @function
 * @param { Object } props Component props
 * @returns { JSX.Element } element Header element
 */
const Header = ({ title }) => {
  return (
    <header className="top-header">
      <h3 className="title">Top 10 animation movies</h3>
    </header>
  );
}

export default Header;
