import React from 'react';
import { shallow } from 'enzyme';
import Header from '../Header';

let wrapped, title;

beforeEach(() => {
  wrapped = shallow(<Header />);
  title = wrapped.find('.title');
});

it('A header element is displayed', () => {
  expect(wrapped.find('header').length).toEqual(1);
});

it('A title is displayed', () => {
  expect(title.length).toEqual(1);
});

it('Title is Top 10 animation movies', () => {
  expect(title.text()).toContain('Top 10 animation movies');
});
