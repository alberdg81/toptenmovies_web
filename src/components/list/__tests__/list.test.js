import React from 'react';
import { shallow } from 'enzyme';
import List from '../List';

let wrapped;
beforeEach(() => {
  wrapped = shallow(<List />)
});

it('has a ul', () => {
  expect(wrapped.find('ul').length === 1);
});

it('has ten list items', () => {
  expect(wrapped.find('li').length === 10);
});
