import React from 'React';
import { shallow } from 'enzyme';
import ListItem from '../ListItem';
import { SUPERMAN_SINOPSIS, SUPERMAN_NAME, SUPERMAN_SRC } from '../../../constants';


let wrapped;

describe('ListItem tests', () => {
  beforeEach(() => {
    wrapped = shallow(<ListItem src={SUPERMAN_SRC} name={SUPERMAN_NAME} sinopsis={SUPERMAN_SINOPSIS} />);
  });

  it('Has an img', () => {
    expect(wrapped.find('img').length).toEqual(1);
  });

  it('Has an anchor', () => {
    expect(wrapped.find('a').length).toEqual(1);
  });

  it('Has a span', () => {
    expect(wrapped.find('p').length).toEqual(1);
  });

  it('Has the correct name', () => {
    expect(wrapped.find('a').text()).toEqual(SUPERMAN_NAME);
  });

  it('Has the correct name', () => {
    expect(wrapped.find('p').text()).toEqual(SUPERMAN_SINOPSIS);
  });

});
