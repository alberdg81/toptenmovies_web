import React from 'react';
import './List.css';

/**
 * Functional component to render a list item
 * @function
 * @param { String } src Thumbnail source
 * @param { String } name Movie name
 * @param { String } sinopsis Movie sinopsis
 * @returns { JSX.Element } element ListItem Component
 */
const ListItem = ({ src, name, sinopsis }) => {
  return (
    <div className="list-item-container">
      <div className="list-img-container">
        <img src={src} className="list-img"/>
      </div>
      <div className="list-description-container">
        <a href="#">{name}</a>
        <p>{sinopsis}</p>
      </div>
    </div>
  )
}
export default ListItem;
