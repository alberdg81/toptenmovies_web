import React, { useEffect, useState } from 'react';
import ListItem from './ListItem';
import { SUPERMAN_SRC, SUPERMAN_NAME, SUPERMAN_SINOPSIS } from '../../constants';
import { readJSONMovies } from '../../actions';
import './List.css';

/**
 * List component to display a list of items
 * @function
 * @returns { JSX.Element } element List Component
 */
const List = () => {
  const [ movies, setMovies ] = useState([]);

  useEffect(() => {
    setTimeout(async () => {
      const movies = await readJSONMovies();
      setMovies(movies);
    }, 50);
  }, []);


  /**
   * Renders the list of movies
   * @function
   * @returns { [JSX.Element] } Array of ListItem
   */
  const renderMovies = () => {
    if (!Array.isArray(movies.data) || movies.data.length === 0) return null;
    return movies.data.map((movie, index) => renderMovieItem(movie, index));
  }

  /**
   * Renders a movie List item
   * @function
   * @returns { JSX.Element } Movie ListItem
   */
  const renderMovieItem = (movie, key) => {
    const { thumbnail, name, sinopsis } = movie;
    console.log(thumbnail)
    return <ListItem src={thumbnail} name={name} sinopsis={sinopsis} key={key}/>
  }

  return (
    <div id="list-container">
      <ul>
        {renderMovies()}
      </ul>
    </div>
  )
}
export default List;
