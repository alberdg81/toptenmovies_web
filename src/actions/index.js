/**
 * Reads list of movies from static JSON file
 * @function
 * @returns { [Object] } movies Array of json movies
 */
export const readJSONMovies = async () => {
  debugger;
  const response = await fetch('movies.json');
  const movies = await response.json();
  return movies;
}
