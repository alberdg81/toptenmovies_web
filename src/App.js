import React from 'react';
import Header from './components/header/Header';
import List from './components/list/List';

import './App.css';

/**
 * Main App component
 * @function
 * @returns { JSX.Element } element App Component
 */
const App = () => {
  return (
    <div className="App">
      <Header />
      <div className="content">
        <List />
      </div>
    </div>

  );
}

export default App;
